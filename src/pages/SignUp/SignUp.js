import React from "react";
import * as Components from "components";
import { Helmet } from "react-helmet";
import messages from "utils/messages";

export default class SignUp extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className="signup-page">
        <Helmet>
          <title>{messages.H_SIGNUP}</title>
          <meta name="description" content={messages.H_DESC_SIGNUP} />
          <link
            rel="alternate"
            href="https://everyday.app/signup"
            hrefLang="en"
          />
        </Helmet>
        <Components.SignUp appleSignIn />
      </div>
    );
  }
}
