import BrowserDtector from "browser-dtector";
import { isExtension } from "utils/extension";

export function getBrowser() {
  const browserDetector = new BrowserDtector(window.navigator.userAgent);
  const browserInfo = browserDetector.getBrowserInfo();
  let browser = browserInfo.name;

  if (browser === "Google Chrome") browser = "chrome";
  if (browser === "Microsoft Edge") browser = "edge";
  if (browser === "Mozilla Firefox") browser = "firefox";
  if (browser === "Safari") browser = "safari";

  return browser;
}

export function getDevice() {
  if (!isExtension()) return "web";
  let browser = getBrowser();

  browser = "we-" + browser;
  return browser;
}
