import React from "react";
import { unsubscribeFromMailing } from "actions/users";
import { connect } from "react-redux";
import messages from "utils/messages";

class UnsubscribeMailing extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      unsubscribed: false,
    };
  }

  componentDidMount() {
    unsubscribeFromMailing(this.props.params).then(() =>
      this.setState({ unsubscribed: true })
    );
  }

  render() {
    return (
      <div className="unsubscribemailing-page">
        {this.state.unsubscribed ? (
          <div className="unsubscribemailing">
            {messages.UNSUBSCRIBED_MAILING}
          </div>
        ) : null}
      </div>
    );
  }
}

export default connect()(UnsubscribeMailing);
