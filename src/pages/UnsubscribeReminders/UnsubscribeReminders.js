import React from "react";
import { unsubscribeReminders } from "actions/users";
import { connect } from "react-redux";
import messages from "utils/messages";

class UnsubscribeReminders extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      unsubscribed: false,
    };
  }

  componentDidMount() {
    const params = {
      token: this.props.params.token,
    };
    switch (this.props.route.path.split("/")[1]) {
      case "unsubscribe-daily-reminders":
        params.reminder = 0;
        unsubscribeReminders(params).then(() =>
          this.setState({ unsubscribed: true })
        );
        break;
      case "unsubscribe-weekly-reminders":
        params.reminder = 1;
        unsubscribeReminders(params).then(() =>
          this.setState({ unsubscribed: true })
        );
        break;
      case "unsubscribe-ds2-reminders":
        params.reminder = 2;
        unsubscribeReminders(params).then(() =>
          this.setState({ unsubscribed: true })
        );
        break;
      case "unsubscribe-all-reminders":
        params.reminder = 3;
        unsubscribeReminders(params).then(() =>
          this.setState({ unsubscribed: true })
        );
        break;
      case "unsubscribe-challenge-reminders":
        params.reminder = 4;
        unsubscribeReminders(params).then(() =>
          this.setState({ unsubscribed: true })
        );
        break;
      default:
        break;
    }
  }

  render() {
    return (
      <div className="unsubscribereminders-page">
        {this.state.unsubscribed ? (
          <div className="unsubscribereminders">
            {messages.UNSUBSCRIBED_REMINDERS}
          </div>
        ) : null}
      </div>
    );
  }
}

export default connect()(UnsubscribeReminders);
