import React from "react";
import { connect } from "react-redux";
import { addHabit, editHabit } from "actions/habits";
import { format, addDays } from "date-fns";
import messages from "utils/messages";
import { stringToLocaltimeDate } from "utils/dates";
import { habits as habitValidation } from "common/validations";
import { showModal } from "actions/modal";

class NewHabitModal extends React.Component {
  static getDayNames() {
    let date = stringToLocaltimeDate("2020-04-20");
    const res = [];
    for (let i = 0; i <= 6; i++) {
      res.push(format(date, "ddd"));
      date = addDays(date, 1);
    }
    return res;
  }

  constructor(props) {
    super(props);

    const habit = props.habit || {};

    this.state = {
      errors: [],
      monday: true,
      tuesday: true,
      wednesday: true,
      thursday: true,
      friday: true,
      saturday: true,
      sunday: true,
      advanced: false,
      ...habit,
    };
  }

  handleNameChange(habit) {
    this.setState({
      name: habit.target.value,
    });
  }

  handleBreakChange() {
    this.setState((prevState) => ({ break_habit: !prevState.break_habit }));
  }

  handleArchivedChange() {
    if (!this.props.user.isPremium) {
      this.props.dispatch(
        showModal("premiumFeature", {
          class: "premiumfeature",
          type: "archive",
        })
      );
    }
    this.setState((prevState) => ({ archived: !prevState.archived }));
  }

  setEveryday() {
    this.setState({
      monday: true,
      tuesday: true,
      wednesday: true,
      thursday: true,
      friday: true,
      saturday: true,
      sunday: true,
    });
  }

  setDayFrequency(day) {
    this.setState((state) => ({
      [day]: !state[day],
    }));
  }

  checkEnter(e) {
    if (e.keyCode === 13) {
      this.addHabit();
    }
  }

  advancedOptions(advanced) {
    this.setState({ advanced });
  }

  isEveryday() {
    const { monday, tuesday, wednesday, thursday, friday, saturday, sunday } =
      this.state;
    if (
      monday &&
      tuesday &&
      wednesday &&
      thursday &&
      friday &&
      saturday &&
      sunday
    ) {
      return true;
    }
    return false;
  }

  confirmDeleteModal(habit) {
    this.props.dispatch(
      showModal("deleteHabit", { title: messages.CONFIRM_DELETE_HABIT, habit })
    );
  }

  addHabit() {
    if (this.submitted) return;

    const oldHabit = this.props.habit || {};
    const user_id = oldHabit.user_id || this.props.user.id;

    const newHabit = { ...oldHabit, ...this.state };
    newHabit.user_id = user_id;

    if (newHabit.archived) newHabit.position = 99999;
    if (oldHabit.archived && !newHabit.archived)
      newHabit.position = this.props.numNonArchivedHabits;

    const validationErrors = habitValidation(newHabit);
    if (validationErrors) {
      this.setState({ errors: validationErrors });
      return;
    }

    this.submitted = true;

    if (!oldHabit.id) this.props.dispatch(addHabit(newHabit));
    else {
      this.props.dispatch(editHabit(newHabit));
    }
    this.props.onHide();
  }

  renderFrequencies() {
    const dayLabels = this.getDayNames();
    const res = [];
    const dayNames = [
      "monday",
      "tuesday",
      "wednesday",
      "thursday",
      "friday",
      "saturday",
      "sunday",
    ];

    for (const [i, dayLabel] of dayLabels.entries()) {
      const dayName = dayNames[i];
      const className =
        "frequency-cell " + (this.state[dayName] ? "frequency-marked" : null);
      res.push(
        <div
          className="frequency-block"
          onClick={this.setDayFrequency.bind(this, dayName)}
        >
          <span className="frequency-day-name">{dayLabel}</span>
          <div className={className} />
        </div>
      );
    }

    return res;
  }

  render() {
    const habit = this.props.habit || {};
    return (
      <div className="newHabit">
        {this.state.errors.length ? (
          <ul className="errors">
            {this.state.errors.map((error) => (
              <li key={error}> {error.message} </li>
            ))}
          </ul>
        ) : null}
        <div className="field">
          <a className="label">{messages.HABIT_NAME}</a>
          <input
            className="habitname"
            type="text"
            name="habitname"
            placeholder={messages.HABIT_NAME_PH}
            value={this.state.name}
            onChange={this.handleNameChange.bind(this)}
            onKeyDown={this.checkEnter.bind(this)}
            maxLength="40"
            autoFocus
          />
        </div>
        <div className="field">
          {this.state.advanced ? (
            <div className="advanced-options">
              <a
                className="ao-title"
                onClick={this.advancedOptions.bind(this, false)}
              >
                <i className="ion-arrow-down-b" />
                <span>{messages.ADVANCED_OPTIONS}</span>
              </a>
              <div className="option">
                <input
                  type="checkbox"
                  id="break_habit"
                  checked={this.state.break_habit}
                  onChange={this.handleBreakChange.bind(this)}
                />
                <label htmlFor="break_habit">{messages.AO_BREAK_HABIT}</label>
                <a className="explanation">{messages.AO_BREAK_HABIT_EXP}</a>
              </div>
              <div className="option frequency">
                <div>
                  <span className="frequency-title">
                    {messages.AO_FREQUENCY}
                  </span>
                  <input
                    type="checkbox"
                    id="everyday"
                    checked={this.isEveryday()}
                    onChange={this.setEveryday.bind(this)}
                  />
                  <label htmlFor="everyday">
                    {messages.AO_FREQUENCY_EVERYDAY}
                  </label>
                </div>
                <div className="frequency-blocks">
                  {this.renderFrequencies()}
                </div>
                <span className="frequency-exp">
                  {messages.AO_FREQUENCY_EXP}
                </span>
              </div>
              {habit.id && (
                <div className="option archived">
                  <input
                    type="checkbox"
                    id="archived"
                    checked={this.state.archived}
                    onChange={this.handleArchivedChange.bind(this)}
                  />
                  <label htmlFor="archived">{messages.AO_ARCHIVED}</label>
                  <a className="explanation">{messages.AO_ARCHIVED_EXP}</a>
                </div>
              )}
            </div>
          ) : (
            <div className="advanced-options">
              <a
                className="ao-title"
                onClick={this.advancedOptions.bind(this, true)}
              >
                <i className="ion-arrow-right-b" />
                <span>{messages.ADVANCED_OPTIONS}</span>
              </a>
            </div>
          )}
        </div>
        <div className="field">
          {habit.id ? (
            <a
              className="delete-button"
              onClick={this.confirmDeleteModal.bind(this, this.props.habit)}
            >
              <i className="ion-close-round" />
              <span>{messages.DELETE}</span>
            </a>
          ) : null}
          <a className="modal-button" onClick={this.addHabit.bind(this)}>
            {habit.id ? messages.EDIT_HABIT : messages.ADD_HABIT}
          </a>
          <a className="modal-button cancel" onClick={this.props.onHide}>
            {messages.CANCEL}
          </a>
        </div>
        <div className="clear" />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    numNonArchivedHabits: state.habits.habits.length,
  };
}

export default connect(mapStateToProps)(NewHabitModal);
