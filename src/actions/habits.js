import request from "utils/request";
import { subDays, format, differenceInCalendarDays } from "date-fns";
import { fetchDatesSuccess, autoskip } from "actions/dates";
import { calculateNewStreaks } from "actions/stats";
import constants from "common/constants";
import history from "utils/history";

export function fetchHabitsSuccess(habits, habitsHash) {
  return {
    type: "FETCH_HABITS_SUCCESS",
    habits,
    habitsHash,
  };
}

export function fetchHabitSuccess(habit) {
  return {
    type: "FETCH_HABIT_SUCCESS",
    habit,
  };
}

export function addHabitSuccess(habit) {
  return {
    type: "ADD_HABIT_SUCCESS",
    habit,
  };
}

export function editHabitSuccess(habit) {
  return {
    type: "EDIT_HABIT_SUCCESS",
    habit,
  };
}

export function deleteHabitSuccess(habit_id) {
  return {
    type: "DELETE_HABIT_SUCCESS",
    habit_id,
  };
}

export function moveHabit(dragIndex, hoverIndex, dragHabit) {
  return {
    type: "MOVE_HABIT",
    dragIndex,
    hoverIndex,
    dragHabit,
  };
}

// Actions to handle queueing to avoid overwriting when dates get fetched
export function fetchingHabits() {
  return {
    type: "FETCHING_HABITS",
  };
}

export function notFetchingHabits() {
  return {
    type: "NOT_FETCHING_HABITS",
  };
}

export function clearQueue() {
  return {
    type: "CLEAR_QUEUE",
  };
}

export function createAutoSkips(habitId) {
  return async (dispatch, getState) => {
    const state = getState();
    const habits = state.habits.habits
      .filter((id) => !habitId || id === habitId)
      .map((id) => state.habitsEntities[id]);

    const lastAction = new Date(state.user.last_action);
    let numDays = differenceInCalendarDays(new Date(), lastAction);
    const today = new Date();
    while (numDays >= 0) {
      const currentDate = subDays(today, numDays);
      const dateStr = format(currentDate, "YYYY-MM-DD");
      const dayName = format(currentDate, "dddd").toLowerCase();

      for (const habit of habits) {
        if (
          !habit[dayName] &&
          !(state.dates[habit.id] && state.dates[habit.id][dateStr])
        ) {
          dispatch(
            autoskip({
              date: dateStr,
              habit_id: habit.id,
              device: "web",
            })
          );
        }
      }

      numDays--;
    }
  };
}

export function fetchHabit({ userId, habitId }) {
  const baseDate = subDays(new Date(), constants.numDays);
  const formattedBaseDate = format(baseDate, "YYYY-MM-DD");

  return async (dispatch) => {
    try {
      dispatch(fetchingHabits());
      const res = await request({
        url: `habitsV2?&base_date=${formattedBaseDate}&user_id=${userId}&habit_id=${habitId}`,
        method: "GET",
      });

      const { habitsArray, datesHash } = res;

      const habit = habitsArray[0];
      const habitWithStats = calculateNewStreaks({
        dates: datesHash[habit.id],
        habit,
        baseDate: formattedBaseDate,
      });

      dispatch(
        fetchDatesSuccess({
          dates: datesHash,
          baseDate: formattedBaseDate,
          overwrite: false,
        })
      );
      dispatch(fetchHabitSuccess(habitWithStats));
    } catch (e) {
      history.replace("/");
    }
  };
}

export function fetchHabits() {
  return async (dispatch) => {
    try {
      dispatch(fetchingHabits());

      const baseDate = subDays(new Date(), constants.numDays);
      const formattedBaseDate = format(baseDate, "YYYY-MM-DD");

      const res = await request({
        url: `habitsV2?&base_date=${formattedBaseDate}`,
        method: "GET",
      });

      const { habitsArray, habitsHash, datesHash } = res;

      dispatch(
        fetchDatesSuccess({ dates: datesHash, baseDate: formattedBaseDate })
      );

      const habits = habitsArray.map((habit) => {
        const habitWithStats = calculateNewStreaks({
          dates: datesHash[habit.id],
          habit,
          baseDate: formattedBaseDate,
        });
        habitsHash[habit.id] = habitWithStats;
        return habitWithStats;
      });

      dispatch(fetchHabitsSuccess(habits, habitsHash));
      dispatch(createAutoSkips());
      dispatch({ type: "UPDATE_USER_LAST_ACTION" });
    } catch (e) {
      console.log(e);
    }
  };
}

export function addHabit(habit) {
  return (dispatch) => {
    request({
      url: "habits",
      method: "POST",
      body: habit,
    }).then((newHabit) => {
      const newerHabit = newHabit;
      newerHabit.currentStreak = 0;
      newerHabit.longestStreak = 0;
      newerHabit.weekCount = 0;
      newerHabit.monthCount = 0;
      newerHabit.yearCount = 0;
      newerHabit.weekComplRate = 0;
      newerHabit.monthComplRate = 0;
      newerHabit.yearComplRate = 0;
      newerHabit.totalCount = 0;
      newerHabit.baseLongestStreak = 0;
      newerHabit.baseCurrentStreak = 0;
      newerHabit.baseYearCount = 0;
      newerHabit.baseMonthCount = 0;
      newerHabit.baseTotalCount = 0;
      newerHabit.skipMonthCount = 0;
      newerHabit.skipYearCount = 0;
      newerHabit.monthBase = 0;
      newerHabit.yearBase = 0;
      dispatch(addHabitSuccess(newerHabit));
      dispatch(createAutoSkips(newerHabit.id));
    });
  };
}

export function editHabit(habit) {
  console.log(habit);
  return (dispatch, getState) =>
    request({
      url: "habits/" + habit.id,
      method: "PUT",
      body: habit,
    }).then(() => {
      const state = getState();
      const oldHabit = state.habitsEntities[habit.id];

      if (oldHabit.archived && !habit.archived) {
        dispatch({
          type: "UNARCHIVE",
          id: habit.id,
        });
      }

      if (!oldHabit.archived && habit.archived) {
        dispatch({
          type: "ARCHIVE",
          id: habit.id,
        });
      }

      dispatch(editHabitSuccess(habit));
      dispatch(createAutoSkips(habit.id));

      if (
        oldHabit.color !== habit.color ||
        oldHabit.break_habit !== habit.break_habit
      )
        dispatch({
          type: "UPDATE_HABIT_COLOR",
          habit,
        });
    });
}

export function deleteHabit(habit_id) {
  return (dispatch) => {
    request({
      url: "habits/" + habit_id,
      method: "DELETE",
    }).then(() => {
      dispatch(deleteHabitSuccess(habit_id));
    });
  };
}
