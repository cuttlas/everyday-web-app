import React from "react";
import * as Components from "components";

export default class Login extends React.Component {
  render() {
    return (
      <div className="login-page">
        <Components.LogIn />
      </div>
    );
  }
}
