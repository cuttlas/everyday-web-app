import moment from "moment";
import constants from "common/constants";

function isWeekend(date) {
  return date.day() === 0 || date.day() === 6;
}

export function calculateNewStreaks({ habit, dates = {}, baseDate }) {
  const newHabit = { ...habit };

  // Calculating longestStreak (longest in frontend (longest than 365...?))
  // Calculating counts and completion rates
  let currentStreak = 0;
  let longestStreak = 0;
  let weekCount = 0;
  let monthCount = 0;
  let yearCount = 0;
  let weekBase = 0;
  let monthBase = 0;
  let yearBase = 0;
  let totalCount = 0;
  for (
    let currentDate = moment(baseDate),
      weekBaseDate = moment().subtract(7, "days"),
      monthBaseDate = moment().subtract(30, "days"),
      i = 0,
      streak = habit.baseCurrentStreak,
      initialDate;
    i <= constants.numDays;
    currentDate.add(1, "days"), i++
  ) {
    // CURRENT/LONGEST STREAK
    // if date has an entry
    if (dates && dates[currentDate.format("YYYY-MM-DD")]) {
      // if date marked increment streak
      if (dates[currentDate.format("YYYY-MM-DD")] > 0) {
        streak++;
        totalCount++;
      }

      if (!initialDate) initialDate = currentDate;
    } else if (!(i === constants.numDays)) {
      streak = 0;
    }

    currentStreak = streak;
    if (currentStreak > longestStreak) longestStreak = currentStreak;

    // WEEK COUNT
    // Miro que data sigui posterior als últims 7 dies
    // i a partir de la primera data marcada
    if (
      currentDate.isAfter(weekBaseDate) &&
      initialDate &&
      currentDate.isSameOrAfter(initialDate)
    ) {
      if (!(dates[currentDate.format("YYYY-MM-DD")] < 0)) weekBase++;
      if (dates[currentDate.format("YYYY-MM-DD")] > 0) weekCount++;
    }

    // MONTH COUNT
    if (
      currentDate.isAfter(monthBaseDate) &&
      initialDate &&
      currentDate.isSameOrAfter(initialDate)
    ) {
      if (!(dates[currentDate.format("YYYY-MM-DD")] < 0)) monthBase++;
      if (dates[currentDate.format("YYYY-MM-DD")] > 0) monthCount++;
    }

    // YEAR COUNT
    if (initialDate && currentDate.isSameOrAfter(initialDate)) {
      if (!(dates[currentDate.format("YYYY-MM-DD")] < 0)) yearBase++;
      if (dates[currentDate.format("YYYY-MM-DD")] > 0) yearCount++;
    }
  }

  newHabit.totalCount = habit.baseTotalCount
    ? habit.baseTotalCount + totalCount
    : totalCount;
  newHabit.currentStreak = currentStreak;
  newHabit.longestStreak =
    habit.baseLongestStreak > longestStreak
      ? habit.baseLongestStreak
      : longestStreak;
  newHabit.weekCount = weekCount;
  newHabit.monthCount = monthCount;
  newHabit.yearCount = yearCount;
  newHabit.weekComplRate = Math.round((weekCount / weekBase) * 100) || 0;
  newHabit.monthComplRate = Math.round((monthCount / monthBase) * 100) || 0;
  newHabit.yearComplRate = Math.round((yearCount / yearBase) * 100) || 0;

  return newHabit;
}
