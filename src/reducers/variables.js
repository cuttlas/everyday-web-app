const initialState = {
  theme: "light",
};

const variables = (state = initialState, action) => {
  switch (action.type) {
    case "CHANGE_THEME":
      return {
        theme: action.theme,
      };
    default:
      return state;
  }
};

export default variables;
