import React from "react";
import { connect } from "react-redux";
import messages from "utils/messages";
import { signup } from "actions/auth";
import { isPopupExtension } from "utils/extension";
import config from "config";

class SignUp extends React.Component {
  static signInWithApple() {
    window.location.replace(
      // eslint-disable-next-line
      `https://appleid.apple.com/auth/authorize?client_id=app.everyday.web&redirect_uri=${config.appleRedirectUri}&response_type=code&scope=email name&response_mode=form_post`
    );
  }

  constructor(props) {
    super(props);

    this.state = {
      errors: [],
      message: "",
      notfree: false,
    };
  }

  onSubmit(event) {
    event.preventDefault();
    this.props
      .dispatch(
        signup({
          name: this.nameInput.value,
          email: this.emailInput.value,
          password: this.passInput.value,
          signup_device: "web",
        })
      )
      .then(
        () => null,
        (errors) => this.setState({ errors })
      );
  }

  resetMessage() {
    this.setState({ message: "" });
  }

  whyNotFree() {
    this.setState((prevState) => ({ notfree: !prevState.notfree }));
  }

  render() {
    return (
      <div className="signup">
        <h2>{messages.SIGNUP_TITLE}</h2>
        {this.state.message.length ? (
          <div className="success-message">{this.state.message}</div>
        ) : (
          <div className="signup-form-wrapper">
            <div className="price-message">
              <div className="trial">{messages.FREE}</div>
              <div>{messages.FOREVER}</div>
            </div>
            {this.state.errors.length ? (
              <ul className="errors">
                {this.state.errors.map((error) => (
                  <li key={error}> {error} </li>
                ))}
              </ul>
            ) : null}
            <form className="signup-form" onSubmit={this.onSubmit.bind(this)}>
              <div className="form-row">
                <input
                  ref={(input) => {
                    this.nameInput = input;
                  }}
                  className="name signup-input"
                  autoComplete="name"
                  type="name"
                  name="name"
                />
                <label className="text-label">{messages.S_NAME}</label>
              </div>
              <div className="form-row">
                <input
                  ref={(input) => {
                    this.emailInput = input;
                  }}
                  className="email signup-input"
                  autoComplete="username"
                  type="email"
                  name="email"
                />
                <label className="text-label">{messages.S_EMAIL}</label>
              </div>
              <div className="form-row">
                <input
                  ref={(input) => {
                    this.passInput = input;
                  }}
                  className="password signup-input"
                  autoComplete="new-password"
                  type="password"
                  name="password"
                />
                <label className="text-label">{messages.S_PASSWORD}</label>
              </div>
              <div className="terms">
                <a>
                  <p>{messages.CONDITIONS}</p>
                  <a
                    href="https://everyday.app/terms"
                    target="_blank"
                    rel="noreferrer"
                    className="link"
                  >
                    {messages.CONDITIONS_0}
                  </a>
                  {messages.CONDITIONS_1}
                  <a
                    href="https://everyday.app/privacy"
                    target="_blank"
                    rel="noreferrer"
                    className="link"
                  >
                    {messages.CONDITIONS_2}
                  </a>
                </a>
              </div>
              <button type="submit" className="signup-button">
                {messages.SIGNUP}
              </button>
            </form>
            {this.props.appleSignIn && !isPopupExtension() && (
              <div className="apple-signin-button">
                <div className="or">
                  <a>{messages.OR}</a>
                </div>
                <img
                  onClick={this.signInWithApple}
                  alt="apple"
                  src="https://appleid.cdn-apple.com/appleid/button?type=continue&border_radius=0&width=200&height=30&scale=2"
                />
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

// <a className="whynot" onClick={this.whyNotFree.bind(this)}>
//  {messages.WHYNOTFREE}
// </a>
// {this.state.notfree ? (
//   <div className="because">
//     <p>{messages.BECAUSE}</p>
//     <p>{messages.BECAUSE_2}</p>
//     <p>{messages.BECAUSE_3}</p>
//     <p>
//       {messages.BECAUSE_4}
//       <a
//         className="link"
//         href="http://web.archive.org/web/20170628224824/https://blog.pinboard.in/2011/12/don_t_be_a_free_user/" // eslint-disable-line
//         target="_blank" rel="noreferrer"
//       >
//         {messages.BECAUSE_5}
//       </a>
//       {messages.BECAUSE_6}
//       <a
//         className="link"
//         href="mailto:joan@everyday.app"
//         target="_blank" rel="noreferrer"
//       >
//         {messages.BECAUSE_7}
//       </a>
//     </p>
//   </div>
// ) : null}

function mapStateToProps(state) {
  return {
    errors: state.errors,
  };
}

export default connect(mapStateToProps)(SignUp);
