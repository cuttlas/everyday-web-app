import request from "utils/request";

export async function sendEvent(event) {
  request({
    url: "events",
    method: "POST",
    body: event
  });
}
