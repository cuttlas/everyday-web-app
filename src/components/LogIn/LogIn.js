import React from "react";
import messages from "utils/messages";
import { login } from "actions/auth";
import { isPopupExtension } from "utils/extension";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import config from "config";

class LogIn extends React.Component {
  static signInWithApple() {
    // eslint-disable-next-line
    const url = `https://appleid.apple.com/auth/authorize?client_id=app.everyday.web&redirect_uri=${config.appleRedirectUri}&response_type=code&scope=name email&response_mode=form_post`;
    if (isPopupExtension()) {
      setTimeout(() => {
        window.open(
          url,
          "height=500,width=400,left=600,top=200,status=no,location=no,toolbar=no,menubar=no"
        );
      }, 2000);
    } else window.location.replace(url);
  }

  onSubmit(event) {
    event.preventDefault();
    this.props.dispatch(
      login({
        email: this.emailInput.value,
        password: this.passInput.value,
      })
    );
  }

  render() {
    const urlParams = new URLSearchParams(window.location.search);
    const confirm = urlParams.get("confirm");

    if (confirm)
      this.props.errors.push(
        "Your account has been verified. You can now log in."
      );

    const forgotUrl = config.landingUrl + "/forgot";
    return (
      <div className="login">
        <h2>{messages.LOGIN_TITLE}</h2>
        <div className="login-form-wrapper">
          {this.props.errors.length ? (
            <ul className="errors">
              {this.props.errors.map((error) => (
                <li key={error}> {error} </li>
              ))}
            </ul>
          ) : null}
          <form className="login-form" onSubmit={this.onSubmit.bind(this)}>
            <div className="form-row">
              <input
                ref={(input) => {
                  this.emailInput = input;
                }}
                className="email login-input"
                autoComplete="username"
                type="email"
                name="email"
              />
              <label className="text-label">{messages.S_EMAIL}</label>
            </div>
            <div className="form-row">
              <input
                ref={(input) => {
                  this.passInput = input;
                }}
                className="password login-input"
                autoComplete="current-password"
                type="password"
                name="password"
              />
              <label className="text-label">{messages.S_PASSWORD}</label>
            </div>
            <button type="submit" className="login-button">
              {messages.LOGIN}
            </button>
          </form>
          <a href={forgotUrl} className="forgot">
            {messages.L_FORGOT}
          </a>
          <Link to="/signup" className="signuplink">
            {messages.L_SIGNUP}
          </Link>
          <span>
            <div className="or">
              <a>{messages.OR}</a>
            </div>
            <div className="apple-signin-button">
              <img
                onClick={this.signInWithApple}
                alt="apple"
                // eslint-disable-next-line
                src="https://appleid.cdn-apple.com/appleid/button?type=continue&border_radius=0&width=200&height=30&scale=2"
              />
            </div>
          </span>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    errors: state.errors,
  };
}

export default connect(mapStateToProps)(LogIn);
