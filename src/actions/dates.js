import request from "utils/request";
import { updateStreak } from "actions/stats";
import { editHabitSuccess } from "actions/habits";
import { showModal } from "actions/modal";
import { isExtension } from "utils/extension";
import { getDevice } from "utils/browser";
import * as storage from "utils/storage";
import { isBefore, addDays, format } from "date-fns";

export function fetchDatesSuccess({ dates, baseDate, overwrite = true }) {
  return {
    type: "FETCH_DATES_SUCCESS",
    dates,
    baseDate,
    overwrite,
  };
}

export function markSuccess(date, habit) {
  return {
    type: "MARK_SUCCESS",
    date,
    habit,
  };
}

export function unmarkSuccess(date, habit) {
  return {
    type: "UNMARK_SUCCESS",
    date,
    habit,
  };
}

export function skipSuccess(date, habit) {
  return {
    type: "SKIP_SUCCESS",
    date,
    habit,
  };
}

export function autoskip({ date, habit_id }) {
  return async (dispatch, getState) => {
    try {
      const state = getState();
      dispatch(
        skipSuccess(date, state.habitsEntities[habit_id], state.dates[habit_id])
      );
      dispatch(updateStreak(habit_id));
      await request({
        url: "autoskip",
        method: "POST",
        action: "MARK",
        body: {
          date,
          habit_id,
          device: getDevice(),
        },
        habitId: habit_id,
      });
    } catch (e) {
      console.log("autoskip failed", e);
    }
  };
}

function addToQueue(action) {
  return {
    type: "ADD_TO_QUEUE",
    action,
  };
}

export function mark(date, habit_id, os) {
  const dateObject = {
    date,
    habit_id,
    device: getDevice(),
  };
  return (dispatch, getState) => {
    const state = getState();

    if (state.queue.fetchingHabits) {
      dispatch(addToQueue({ action: "mark", date, habit_id, os }));
      return;
    }
    // Optimistic update mark a habit
    dispatch(markSuccess(date, state.habitsEntities[habit_id]));

    // Retrieving new current and longest Streaks from state
    const habitBackup = state.habitsEntities[habit_id];

    // the markSuccess dispatch affects state and need updated to calculate
    dispatch(updateStreak(habit_id));

    const habit = state.habitsEntities[habit_id];
    const review = storage.get("edc_review");

    if (habit.currentStreak >= 5) {
      if (isExtension()) {
        if (
          !review ||
          (!review.hasReviewed &&
            isBefore(
              addDays(new Date(review.lastReviewRequest), 7),
              new Date()
            ))
        ) {
          dispatch(showModal("review"));
          storage.set({
            key: "edc_review",
            persist: true,
            value: { lastReviewRequest: format(new Date(), "YYYY-MM-DD") },
          });
        }
      }
    }

    request({
      url: "mark",
      method: "POST",
      body: dateObject,
    })
      .then((res) => {
        if (res.jwtToken) {
          const user = storage.get("edc_user");
          user.jwtToken = res.jwtToken;
          storage.set({
            key: "edc_user",
            value: user,
            persist: true,
          });
        }
      })
      .catch(() => {
        dispatch(unmarkSuccess(date, habitBackup));
        dispatch(editHabitSuccess(habitBackup));
      });
  };
}

export function unmark(date, habit_id) {
  const dateObject = {
    date,
    habit_id,
  };
  return (dispatch, getState) => {
    const state = getState();

    if (state.queue.fetchingHabits) {
      dispatch(addToQueue({ action: "unmark", date, habit_id }));
      return;
    }

    // Optimistic update unmark a habit
    dispatch(unmarkSuccess(date, state.habitsEntities[habit_id]));

    // Retrieving new current and longest Streaks from state
    const habitBackup = state.habitsEntities[habit_id];

    // the unmarkSuccess dispatch affects state and need updated to calculate
    dispatch(updateStreak(habit_id));

    request({
      url: "unmark",
      method: "POST",
      body: dateObject,
    })
      .then()
      .catch(() => {
        dispatch(skipSuccess(date, habitBackup));
        dispatch(editHabitSuccess(habitBackup));
      });
  };
}

export function skip(date, habit_id) {
  const dateObject = {
    date,
    habit_id,
  };
  return (dispatch, getState) => {
    const state = getState();

    if (state.queue.fetchingHabits) {
      dispatch(addToQueue({ action: "skip", date, habit_id }));
      return;
    }

    const infoModals = storage.get("edc_info_modals");

    if (!infoModals || !infoModals.skip) {
      dispatch(showModal("skip", { delay: 500 }));
      storage.set({
        key: "edc_info_modals",
        persist: true,
        value: { ...infoModals, skip: true },
      });
    }

    // Optimistic update skip a habit
    dispatch(skipSuccess(date, state.habitsEntities[habit_id]));

    // Retrieving new current and longest Streaks from state
    const habitBackup = state.habitsEntities[habit_id];

    // the skipSuccess dispatch affects state and need updated to calculate
    dispatch(updateStreak(habit_id));

    request({
      url: "skip",
      method: "POST",
      body: dateObject,
    })
      .then()
      .catch(() => {
        dispatch(markSuccess(date, habitBackup));
        dispatch(editHabitSuccess(habitBackup));
      });
  };
}
