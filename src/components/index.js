/**
 * Entry point for our components directory so we can
 * import them more easily elsewhere
 *
 * ie: import {Nav, Menu} from 'components'
 */

import Nav from "./Nav/Nav";
import Menu from "./Menu/Menu";
import Header from "./Header/Header";
import Banner from "./Banner/Banner";
import Footer from "./Footer/Footer";
import Row from "./Row/Row";
import Cell from "./Cell/Cell";
import Board from "./Board/Board";
import BoardFooter from "./BoardFooter/BoardFooter";
import Stats from "./Stats/Stats";
import Modal from "./Modal/Modal";
import NewHabitModal from "./NewHabitModal/NewHabitModal";
import ConfirmDeleteModal from "./ConfirmDeleteModal/ConfirmDeleteModal";
import MenuItem from "./MenuItem/MenuItem";
import LogIn from "./LogIn/LogIn";
import SignUp from "./SignUp/SignUp";
import NoHabits from "./NoHabits/NoHabits";
import SubHeader from "./SubHeader/SubHeader";
import Notification from "./Notification/Notification";
import SubscriptionModal from "./SubscriptionModal/SubscriptionModal";
import ReviewModal from "./ReviewModal/ReviewModal";
import Tips from "./Tips/Tips";
import WebExtensions from "./WebExtensions/WebExtensions";
import ConfirmDeactivateModal from "./ConfirmDeactivateModal/ConfirmDeactivateModal";
import SubNav from "./SubNav/SubNav";
import Icon from "./Icon/Icon";
import PremiumFeatureModal from "./PremiumFeatureModal/PremiumFeatureModal";
import SkipModal from "./SkipModal/SkipModal";
import OnboardingModal from "./Onboarding/OnboardingModal/OnboardingModal";
import CheckoutForm from "./CheckoutForm/CheckoutForm";
// Settings
import MailingList from "./Settings/MailingList/MailingList";
import DarkModeSwitch from "./Settings/DarkModeSwitch/DarkModeSwitch";
import ExportData from "./Settings/ExportData/ExportData";
import EmailReminders from "./Settings/EmailReminders/EmailReminders";
import Account from "./Settings/Account/Account";
import Subscription from "./Settings/Subscription/Subscription";
import DeleteAccount from "./Settings/DeleteAccount/DeleteAccount";

export {
  Nav,
  Menu,
  Header,
  Banner,
  Footer,
  Board,
  BoardFooter,
  Row,
  Cell,
  Stats,
  Modal,
  NewHabitModal,
  ConfirmDeleteModal,
  MenuItem,
  LogIn,
  SignUp,
  NoHabits,
  SubHeader,
  Notification,
  SubscriptionModal,
  Tips,
  WebExtensions,
  MailingList,
  DarkModeSwitch,
  ExportData,
  EmailReminders,
  Account,
  Subscription,
  DeleteAccount,
  ConfirmDeactivateModal,
  SubNav,
  Icon,
  PremiumFeatureModal,
  ReviewModal,
  SkipModal,
  OnboardingModal,
  CheckoutForm,
};
