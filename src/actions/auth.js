import request from "utils/request";
import * as storage from "utils/storage";
import { fetchHabits } from "actions/habits";
import { initPromo } from "actions/promos";
import history from "utils/history";
import { CrossStorageClient } from "cross-storage";
import { isExtension } from "utils/extension";
import config from "config";

// import { persistor } from 'stores';

export function loginSuccess(user) {
  return {
    type: "LOGIN_SUCCESS",
    user,
  };
}

export function logoutSuccess() {
  return {
    type: "LOGOUT",
  };
}

export function loginFailed(errors) {
  return {
    type: "LOGIN_ERROR",
    errors,
  };
}

export function updateUserSuccess(user) {
  return {
    type: "UPDATE_USER_SUCCESS",
    user,
  };
}

export function setLoggedUserFromQueryParam(userParam) {
  return (dispatch) => {
    const user = JSON.parse(atob(userParam));
    storage.set({
      key: "edc_user",
      value: user,
      persist: true,
    });

    // tell landing app user is logged in
    if (!isExtension() && process.env.NODE_ENV === "production") {
      const crossStorage = new CrossStorageClient(config.externalAppUrl);
      crossStorage
        .onConnect()
        .then(() => crossStorage.set("edc_user", JSON.stringify(user)));
    }
    dispatch(loginSuccess(user));
    dispatch(fetchHabits());

    history.replace("/?social=true");
  };
}

export function login(params) {
  return (dispatch) =>
    request({
      url: `login`,
      method: "POST",
      body: { ...params, os: "web" },
    }).then(
      (res) => {
        const user = Object.assign(res);

        storage.set({
          key: "edc_user",
          value: user,
          persist: true,
        });

        // tell landing app user is logged in
        if (!isExtension() && process.env.NODE_ENV === "production") {
          const crossStorage = new CrossStorageClient(config.externalAppUrl);
          crossStorage
            .onConnect()
            .then(() => crossStorage.set("edc_user", JSON.stringify(user)));
        }
        dispatch(loginSuccess(user));
        dispatch(initPromo());

        // dispatch(showModal("onboarding"));

        history.replace("/");
        window.location.reload();
      },
      (errors) => dispatch(loginFailed(errors))
    );
}

export function signup(params) {
  const urlParams = new URLSearchParams(window.location.search);
  const promo = urlParams.get("promo");
  const ref = urlParams.get("ref");

  const source = ref || document.referrer;
  // return console.log(promo);

  return (dispatch) =>
    request({
      url: `signup?promo=${promo}`,
      method: "POST",
      body: {
        ...params,
        device_os: isExtension() ? "webextension" : "web",
        promo,
        source,
      },
    }).then((res) => {
      const user = Object.assign(res);
      user.token = btoa(
        unescape(encodeURIComponent(`${params.email}:${params.password}`))
      );

      storage.set({
        key: "edc_user",
        value: user,
        persist: true,
      });

      dispatch(loginSuccess(user));
      dispatch(initPromo());

      history.replace("/");

      return user;
    });
}

export function logout() {
  return (dispatch) => {
    // storage.remove('edc_user');
    // remove both, edc_user and app state from localStorage
    console.log("isExtension", isExtension());
    storage.clear();
    // persistor.purge();

    // tell landing app user is logged out
    if (!isExtension() && process.env.NODE_ENV === "production") {
      const crossStorage = new CrossStorageClient(config.externalAppUrl);
      crossStorage
        .onConnect()
        .then(() => crossStorage.clear())
        .then(() => {
          window.location.reload();
          dispatch(logoutSuccess());
        });
    } else {
      history.replace("login");
      dispatch(logoutSuccess());
    }
  };
}

export function restore() {
  return (dispatch) => {
    const user = storage.get("edc_user");

    if (user) {
      dispatch(loginSuccess(user));
    }
  };
}

export function confirmEmail() {
  return () => {
    const edc_user = storage.get("edc_user");
    if (!edc_user) {
      history.replace("/login?confirm=true");
      return;
    }
    history.replace("/");
  };
}
