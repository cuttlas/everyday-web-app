import React from "react";
import ReactDOM from "react-dom";
import * as storage from "utils/storage";
import * as Sentry from "@sentry/browser";
import { CrossStorageHub } from "cross-storage";
import config from "config";
import { changeTheme } from "actions/theme";
import { fetchHabits } from "actions/habits";
import { App } from "containers";
import { syncData } from "actions/users";
import { restore, setLoggedUserFromQueryParam } from "actions/auth";
import { initPromo, setSourcePromo } from "actions/promos";
import { sendEvent } from "utils/events";
import { applyTheme } from "utils/themes";
import { isExtension } from "utils/extension";
import store from "./stores";
import reportWebVitals from "./reportWebVitals";

require("styles/base.scss");

ReactDOM.render(<App />, document.getElementById("root"));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

if (!isExtension())
  CrossStorageHub.init([
    {
      origin: config.externalApp,
      allow: ["get", "set", "del", "getKeys", "clear"],
    },
  ]);

Sentry.init({
  dsn: "https://92521b5c8bcd470cb0298889f39ba259@sentry.io/2068974",
});

store.dispatch(restore());
const state = store.getState();
applyTheme(state.variables.theme);

const urlParams = new URLSearchParams(window.location.search);
const promo = urlParams.get("promo");
const userParam = urlParams.get("user");

if (promo) {
  storage.set("promo", promo);
}

if (state.user && state.user.id) {
  const webextension = urlParams.get("webextension") || isExtension();
  const email = urlParams.get("email");

  if (webextension) {
    sendEvent({
      type: "NewWebSession",
      value: "webextension",
    });
  }
  if (email) {
    sendEvent({
      type: "NewWebSession",
      value: email,
    });
  }
}

if (userParam) {
  store.dispatch(setLoggedUserFromQueryParam(userParam));
  const data = { type: "SOCIAL_LOGIN", user: userParam };
  window.postMessage(data, "*");
}

(async () => {
  await store.dispatch(initPromo());
  const social = urlParams.get("social"); // social login
  if (social) {
    store.dispatch(setSourcePromo());
  }
})();
// set user after apple sign in in web extension
if (isExtension()) {
  // const browser = window.chrome ? window.chrome : window.browser;
  if (window.browser) {
    window.browser.storage.local.get(["social_user"]).then((res) => {
      if (res.social_user) {
        store.dispatch(setLoggedUserFromQueryParam(res.social_user));
        window.browser.storage.local.clear();
      }
    });
  } else if (window.chrome) {
    window.chrome.storage.local.get(["social_user"], (res) => {
      if (res.social_user) {
        store.dispatch(setLoggedUserFromQueryParam(res.social_user));
        window.chrome.storage.local.clear();
      }
    });
  } else if (window.safari) {
    const socialUser = window.safari.extension.settings.social_user;
    if (socialUser) {
      store.dispatch(setLoggedUserFromQueryParam(socialUser));
      delete window.safari.extension.settings.social_user;
    }
  }
}

const version = storage.get("edc_state_version");

if (state.user.id && version !== config.storageVersion) {
  store.dispatch(fetchHabits());
  storage.set({
    key: "edc_state_version",
    value: config.storageVersion,
    persist: true,
  });
}

window.addEventListener("storage", (e) => {
  if (e.key === "edc_user" && e.oldValue && !e.newValue) {
    // console.log("reload user");
    window.location.reload();
  }
});

window.addEventListener("visibilitychange", () => {
  if (document.visibilityState === "visible") {
    const currentState = store.getState();
    if (currentState.user.id) store.dispatch(syncData());
    if (
      !currentState.user.isPremium &&
      currentState.variables.theme === "dark"
    ) {
      store.dispatch(changeTheme());
    }
  }
});
