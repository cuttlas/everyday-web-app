import React from "react";
import * as Components from "components";
import * as Pages from "pages";
import config from "config";
import StripeScriptLoader from "react-stripe-script-loader";
import { StripeProvider } from "react-stripe-elements";
import { Provider } from "react-redux";
import { Helmet } from "react-helmet";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import history from "utils/history";
import { isExtension, isPopupExtension } from "utils/extension";
import { useSelector } from "react-redux";
import store from "../../stores";

function PrivateRoute({ children, ...rest }) {
  let user = useSelector((state) => state.user);
  return (
    <Route
      {...rest}
      render={({ location }) =>
        user.id ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}

function App() {
  return (
    <Provider store={store}>
      <Router history={history}>
        <div className="app">
          <Helmet />
          <div className="wrap">
            <Components.Modal />
            <Components.Banner />
            <Components.Nav />
            <div className="content">
              <Switch>
                <PrivateRoute exact path="/">
                  <Pages.Board />
                </PrivateRoute>
                <PrivateRoute path="/archive">
                  <Pages.Board />
                </PrivateRoute>
                <Route path="/subscribe">
                  <Pages.Board />
                </Route>
                <Route path="/login">
                  <Pages.LogIn />
                </Route>
                <Route path="/signup">
                  <Pages.SignUp />
                </Route>
                <Route path="/unsubscribe-mailing/:token">
                  <Pages.UnsubscribeMailing />
                </Route>
                <Route path="/unsubscribe-daily-reminders/:token">
                  <Pages.UnsubscribeReminders />
                </Route>
                <Route path="/unsubscribe-weekly-reminders/:token">
                  <Pages.UnsubscribeReminders />
                </Route>
                <Route path="/unsubscribe-ds2-reminders/:token">
                  <Pages.UnsubscribeReminders />
                </Route>
                <Route path="/unsubscribe-all-reminders/:token">
                  <Pages.UnsubscribeReminders />
                </Route>
                <Route path="/unsubscribe-challenge-reminders/:token">
                  <Pages.UnsubscribeReminders />
                </Route>
                <Route path="/settings">
                  <Pages.Settings />
                </Route>
                <Route path="/tips">
                  <Pages.Tips />
                </Route>
                <Route path="/web-extensions">
                  <Pages.WebExtensions />
                </Route>
                <Route path="/:userId/:habitId">
                  <Pages.Habit />
                </Route>
              </Switch>
            </div>
          </div>
          <Components.Footer />
          <div className="clear" />
        </div>
      </Router>
    </Provider>
  );
}

export default function AppWithProviders() {
  const content = (
    <StripeProvider apiKey={config.stripePublishableKey}>
      <App />
    </StripeProvider>
  );

  return isExtension() || isPopupExtension() ? (
    content
  ) : (
    <StripeScriptLoader
      uniqueId="myUniqueId"
      script="https://js.stripe.com/v3/"
      loader=""
    >
      {content}
    </StripeScriptLoader>
  );
}
