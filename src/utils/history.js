import { createBrowserHistory, createMemoryHistory } from "history";
import { isExtension } from "utils/extension";

const currentHistory = isExtension()
  ? createMemoryHistory()
  : createBrowserHistory();

export default currentHistory;
