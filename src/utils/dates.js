export function stringToLocaltimeDate(dateString) {
  // return tZoner.toLocalTZ(dateString);
  const date = new Date(dateString);
  const offset = date.getTimezoneOffset();
  date.setMinutes(date.getMinutes() + offset);
  return date;
}
