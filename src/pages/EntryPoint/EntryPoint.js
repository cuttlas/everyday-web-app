import React from "react";
import { confirmEmail } from "actions/auth";
import { connect } from "react-redux";

class EntryPoint extends React.Component {
  componentDidMount() {
    this.props.dispatch(confirmEmail(this.props.params));
  }

  render() {
    return <div className="entrypoint-page" />;
  }
}

export default connect()(EntryPoint);
