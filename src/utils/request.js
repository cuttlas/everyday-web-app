import fetch from "isomorphic-fetch";
import * as storage from "utils/storage";
import config from "config";
import { getId } from "actions/sync";
import * as qs from "query-string";
import { getDevice } from "utils/browser";

export default function request(params) {
  const user = storage.get("edc_user");
  const body = {
    ...params.body,
    os: getDevice(),
  };
  const headers = {
    "Content-Type": "application/json",
  };
  const queryParams = qs.parse(window.location.search);

  // wololo = user GOD to be able to make habit screenshots
  if (queryParams.wololo) {
    headers.Authorization = "Basic " + btoa(`wololo:${queryParams.wololo}`);
  } else if (user) {
    if (user.jwtToken) headers.Authorization = `${user.jwtToken}`;
    else if (user.token) headers.Authorization = `Basic ${user.token}`;
  }
  if (params.body) {
    body.socketId = getId();
  }

  return fetch(`${config.apiUrl}/${params.url}`, {
    method: params.method || "GET",
    body: ["PUT", "POST"].includes(params.method) ? JSON.stringify(body) : null,
    headers,
  }).then((res) => {
    if (res.status >= 400) {
      return res.json().then((err) => Promise.reject(err.errors));
    }
    const totalHeader = res.headers.get("X-Total");
    const total = totalHeader && parseInt(totalHeader, 10);
    if (total !== null) return res.json().then((data) => ({ data, total }));
    return res.json();
  });
}
