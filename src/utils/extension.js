export function isExtension() {
  return window.isExtension;
}

export function isPopupExtension() {
  return window.isPopupExtension;
}

export function isChromeExtension() {
  // all but safari
  const isSafari = navigator.vendor.indexOf("Apple") > -1;
  return !isSafari && window.isPopupExtension;
}
